

download_deb_only() {
    echo -e "请输入版本号(e.g. \033[32m2.2.5.1\033[0m): "
    version=$1
    read version

    # 检查输入是否为空
    if [ -z "$version" ]; then
        version="2.2.5.1"
    fi

    download_url="https://github.com/v2rayA/v2rayA/releases/download/v$version/installer_debian_x64_$version.deb"

    # 要检查的文件夹路径
    folder="../files/v2rayA"

    # 检查文件夹是否存在
    if [ ! -d "$folder" ]; then
        echo "文件夹不存在，将创建文件夹..."
        mkdir -p "$folder"
        echo "文件夹创建成功！"
    else
        echo "文件夹已存在，无需创建。"
    fi

    curl -L $download_url -o $folder/installer_debian_x64_$version.deb
}

download_deb_only